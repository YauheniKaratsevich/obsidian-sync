# insta

**Paskutinė viltis, kai jau niekas nepadeda.** - Последняя надежда, когда больше ничего не помогает.
**Apsimesk blaivas, nes neįleis į klubą.** - Притворись трезвым, потому что в клуб тебя не пустят.
**Nupirkai viską, išskyrus tai, ko prašė ji?** \- Ты купил все, кроме того, что она просила?
**Nusprendžiau atsikratyti viskou, kas darą muni stora.** - Я решила избавиться от всего, что делает меня толстым.
**Tikros vertybės niekada nesensta.** \- Истинные ценности никогда не стареют.
**Šiuolaikiniai tėvai: "negalima mušti vaikų, tai juos traumuoja".** - Современные родители: «детей бить нельзя, это их травмирует».
**Kam skirtas šis prietaisas?** - Для чего это устройство?
**Ko išmokai iš tėvo?** \- Чему ты научился у своего отца?
**Susikurkite tobulą biurą.** - Создайте идеальный офис.
**Paprastas sprendimas turėti kokybišką geriamą vandenį biure ir namuose.** - Простое решение для обеспечения качественной питьевой воды в офисе и дома.
**Natūralus būdas nekosėti!** - Естественный способ не кашлять!
**Nori sėdėti priekyje** \- Хотите сидеть впереди.
