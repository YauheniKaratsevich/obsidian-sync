# Leksika iš «Lietuvos istorijos repas»

Leksika iš «**Lietuvos istorijos repas»**
mėgautis - наслаждаться, нежиться
mėgautis gyvenimu - наслаждаться жизнью
mėg-autis, ~aujasi, ~avosi
Kalėdos - Рождество
geda - стыд, позор
kaip jums ne gėda! - как вам не стыдно!
man gėda - мне стыдно
nužudytas - убитый
nužudyti - убить, убивать
nužudyti, nužudo, nužudė
paminėtas - упомянутый
paminėti - упомянуть, отметить
paminė-ti, pamini, ~jo
pagonis - язычник
tapti - стать, сделаться
jis tapo gydytoju - он стал врачом
tap-ti, tampa, tapo
įvykis - происшествие, событие
vykti - происходить, вестись, направляться
kas ten vyksta? - что там происходит ?
vykti į pietus - направляться на юг
vyk-ti, ~sta, ~o
valdžia - власть
прийти к власти - ateiti į vaIdžią
tuojau - сейчас
tuoj - сейчас (сокращенно, грубо как «ща»)
tuoj padarysiu - сейчас сделаю
išplėsti - расширить, раздвинуть
išplėsti ribas - расширить границы
išplėsti akis - 1) распахнуть глаза; 2) вытаращить глаза
išplėsti, išplečia, išplėtė
išskirti - выделить, разлучать
išsk-irti, išskiria, ~yrė
netrukus - скоро, в скором времени
pasibelsti - постучаться
pasibelsti į duris - 1) постучать в дверь; 2) постучаться в дверь
bel-sti, ~džia, ~dė
ištremti - сослать, изгнать
ištremti, ištremia, ištrėmė
tremtinys - изгнанник, сосланный
pakarti - повесить (казнь)
pakar-ti, pakaria, pakorė
sukilti - поднять восстание, восстать, подняться, встать
sukil-ti, sukyla, ~o
sukilimas - восстание
liaudies sukilimas - народное восстание
tauta - народ, национальность
kalėjimas - тюрьма
varpas - колокол
aušra - заря
prieš aušrą - до зари
ryto aušra - утренняя заря
leisti - пустить, разрешать, издавать (там еще много значений, мы обсуждали сегодня эти)
\- lei-sti, ~džia, ~do
leisti kraują - пустить кровь
leisti atostogų - пустить в отпуск
leisti spausdinti- разрешить к печати
spauda - пресса
naikinimas - уничтожение, истребление
naikin-ti, ~a, ~o
omenis - понимание
turėti ką omenyje - иметь что-либо в виду, учитывать что-либо
piemuo - пастух
akmuo - камень
sąjūdis - движение
tautinis sąjūdis - национальное движение
susikibti - схватиться, сцепиться
smegenys - мозг, мозги
šaudyti - стрелять
šaudyti iš automato - стрелять из автомата
akimis šaudyti - стрелять глазами
varnas šaudyti - ловить ворон, зевать

šaud-yti, ~o, ~ė

priešakys - перёд; передняя часть

priešakyje - впереди

priešais - спереди, напротив

akis - глаз

priešais + akis = priešakys

užpakalis - зад ; задняя часть

užpakalyje - сзади

mušti - колотить, драться, ударять, бить

muš-ti, ~a, ~ė

mušti Tinderį - Арунас сказал, что это лентяйничать

Но похоже лентяйничать - это mušti dinderį

А тут опять игра слов, dinderį поменяли на Tinderį

tinginiauti - лентяйничать, лодырничать, лениться

tingin-iauti, ~iauja, ~iavo

Ko tu tinginiauji, eik dirbti!
