# Untitled Note

Miela žmona, šiandien negaliu nueiti į parduotuvę. Aš labai pavargau darbe. Skauda rankas ir kojas, o labiausiai skauda galvą. Galbut tu nueik? Prašom nupirk stiklainį medaus, didelį gabalėlį arbuzo, du kilogramus juodųjų serbentu, tris maišelius skrudintų saulėgrąžu su čili preidu, du indelius rūgščios uogienės. Nereikia pirkti duonos, mes turime labai daug namose. Prašom taip pat nepamiršk nupirkti kačiu maisto. Tris pakelius su jautieną arba su lašišą.
Aciu iš anksto.
