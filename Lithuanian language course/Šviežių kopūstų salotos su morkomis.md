# Šviežių kopūstų salotos su morkomis

Vienai porcijai reikia:

* šviežių baltagūžių kopūstų
* venos morkos
* vieno šaukšto cukraus
* puse šaukštelio druskos
* pusantro šaukšto devynių procentų acto
* dvijiu šaukštų augalinio aliejaus

Pirmiausia nuplauname ir supjaustome kopūstus. Tada morkas sutarkuojame rupia tarka. Kopūstus ir morkas sumaišome su druska. Maišome rankomis ir spausdiname kopūstus kad išsiskirtų sultis. Kopūstus paliekame pastovėti apie penkiolika minučių. Tada įdedame cukraus, acto ir gerai išmaišome šaukštu. Paliekame dar penkioms minutems pastovėti kambario temperatūroje. Po to įpilame augalinio aliejuas. Padedame į šaldytuvą dvidešimt minučių ir tada galima valgyti.
