# namų darbai

1. Vakar vakare aš šokau naktiniame klube su draugais. Mokykloje aš šokdavau tautinius šokius. Aš pašoksiu iš džiaugsmo atlyginimo dieną.
2. Aną savaitę tu žaidėi futbolą stadione. Tu žaisdavai futbolą labai gerai mokykloje. Ar tu žaisi futbolą čempionate?
3. Aną mėnesį jie bėgiojo maratoną. Jie visą laiką bėgiodavo po parką. Kitą savaitgalį jie vėl bėgios maratoną.
4. Tris dienas atgal mes žiūrėjome filmą į kina teatrę. Studijuodami universitete mes dažnai žiūrėdavome operas. Ar vakare mes žiūrėsime spektaklį?
5. Vakar per gimtadienį jūs dainavote labai blogai. Ar mokydamasis mokykloje dainuodavote chore? Jei jūs vėl dainuosite, aš išeisiu.

Aš studijavau Baltarusijos valstybiniame informatikos ir radioelektronikos universitete Minske dvylikta metų atgal. Aš studijavau radijo elektronines sistemas. Dabar dirbu programuotoju EPAM sistemose.
