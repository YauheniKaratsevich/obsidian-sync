# Namu darbas: Ka jus darote savaitgali?

Šeštadienį as lik**a**u namie. Daug mieg**o**jau ir žiur**e**jau televizoriaus. Net neiš**ė**jo iš namų. Tada sekmadienį sutvark**ia**u savo darbą stalą. Pakeičiau el**e**ktros lizdą s**ie**noje ir pakab**i**nau lentyną. Skaič**ia**u str**ai**psnius internet**e**. Nusipirkau k**e**letą pr**e**kių internetu (drabuzių ir žaislų katems). Vakare mes ejome su zmoną pasivaikšoti.

Šeštadienį rite mes su zmoną važavome į prekybos centrą nusipirkti kažkokių vaistų. Ten papusričavome ir grižome namo. Sekmadeni buvo mano gimtadienis. Mes su draugais ejome į restoraną. Dovanu aš gavau pinigų ir kūrybingus marškinėlius.

Mano savaitgalis buvo labai įdomus. Šeštadienį su žmona susitikome su žmonos mama ir nuejome į Kaziuko mugę. Mes ne Ilgai vaikščiojome, nes buvo blogas oras. Tada mes su žmona grįžome namo žiūrėti filmą. Sekmadienį nuvažiavome į prekybos centrą nusipirkti naujų patalynes ir rankšluosčių.

Mano savaitgalis buvo labai įdomus ir linksmas. Šeštadeni mane ir žmoną pakviete į dimtadienį . Gimtadienio vakarėlis buvo bare. Mes daug šokome ten. Mes grįžome labai vėlai ir miegojome visa sekmadienį.

Mano savaitgalis buvo nuobodus. Mano žmona išvažiavo į Minską, o aš likau namuose. Šeštadienį aš šiek tiek pasivaikščiojau parke, o paskui visą dieną žiūrėjau televizorių. Šekmadienį aš sutikau savo uošvį oro uoste ir nuvežiau į geležinkelio stotį. Tada vėl visą dieną žiūrėjau televizorių.

Gerai praleidou savaitgalį. Šeštadienj rite aš nuvažiavau į auto plavyklą išvalyti mano automobilį, o vakare mes su žmoną nuvažavome į svečius. Sekmadienį mes žiūrėjome kiną visą dieną. Rite namose, o vakare kinoteatre.

Aš gerai praleidau savaitgalį. Šeštadienį mes buvome svečiuose pas žmonos tevus. Grižome sekmadenį prieš pietus ir iškarto aplankeme Energetikos ir technikos muziejų. Ten buvo idomų. Pamatomė  Tesla ričių šou ir daug kitų eksponatų.

Aš praleidau savo savaitgalį nedomina. Aš buvau kino teatre du kartus: penktadenį vakare ir sekmadenį. Šeštadienį su žmona ėjome pasivaikščioti ir mus užklupo lietus. Teko iškviesti taksi. Visa kitą laiko buvome namuose ir žiūrėjome televizorių.

Mano savaitgalis buvo nuobodus. Mano žmona išvažiavo į Minską, o aš likau namuose. Šeštadienį aš tik nuvažiovau į kirpyklą senamiestyje ir visa kita laiko žiūrėjau filmus. Sekmadienį taip pat buvau namuose ir žiūrėjau televizorių.

Savaitgalį buvau labai užimtas. Šeštadeni automobilyje keičiau padangas ant ratų. Keičiau žiemines padangas vasarinėmis. Tada nusipirkau automatinės plovyklos abonementą ir vakare važiavau plauti automobilį.  Kai grįžau namo, mane sustabdė policija ir patikrino dokumentus (asmens dokumentas, vairuotojo pažymėjimas, transporto priemonės registravimo liudijimas). Sekmadienį visą dieną padėjome seseriai persikraustyti į naujus namus.

Šeštadienį mes vaziavome pas seseri į jos naujus namus padeti suringti (sumantoti) vaikiska batuta ir kepsnine. Mes labai pavargome,kad sekmadeni likome  namose visa diena ir pradejime perjureti seriala. Pirmadeni mes dar karta vajavome pas seseri pavalgiti kepsniu.

Deja, muziejų naktį niekur neišėjau. Šeštadienį ir sekmadienį buvau užsiėmęs. Bet kai kuriuose muziejuose buvau ir anksčiau. Okupacijų ir laisvės kovų muziejuje ir Energetikos ir technikos muziejuje.
