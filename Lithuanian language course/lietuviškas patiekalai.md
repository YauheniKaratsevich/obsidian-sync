# lietuviškas patiekalai

Sveiki,
Aš gyvenu Lietuvoje jau daugiau nei metus  ir galiu duoti keletą patarimų apie lietuviškus nacionalinius patiekalus. Pirmiausia tu turi paragauti šaltibarščių. Tai šalta rožinė sriuba. Reikia valgyti su grietine. Tada turi paragauti cepelinų, tai garsus lietuviškas patiekalas. Skaniau valgyti su spirgučių padažu.  Jei po cepelinų, vis dar alkanas suvalgyk porciją žemaiciu blynų su grybų įdaru. Gali išgerti giros arba alaus. Nepamiršk su šitais gėrimais paragauti  keptos duonos su česnako. Labai skanu. Pusryčiams nusipirk kibinus su vištiena. Labai sotus maistas.
Iki pasimatimo
