# Bare, restorane.

**Ar turite laisvų vietų?** - Есть ли у вас свободные места?
**Ar galima ateiti su gyvūnais?** - Можно ли к вам приходить с животными?
**Ar galima uzimti staliuką prie lango?** \- Можно ли занять столик возле окна?
**Kokia jusu dienos sriuba? Iš ko ji susideda?** - Какой у вас суп дня? Из чего он состоит?
**Ką galite rekomenduoti/pasiūlyti?** - Что вы можете посоветовать/предложить?
**Koks yra populiariausias pagrindinis patiekalas?** - Какое основное блюдо самое популярное?
**Ar šis patiekalas riebus/sūrus/kartus?** - Это блюдо жирное/соленое/горькое?
**Kada patiekalas bus paruoštas?** - Когда блюдо будет готово?
**Ar šiame patiekale yra grybų?** - В этом блюде есть грибы?
**Ar galima nedėti grybu į patiekala?** - Можно ли убрать грибы из блюда?
**Ar galima pašildyti mano patekala, jis šaltas?** - Можно ли подогреть мое блюдо, оно холодное?
**Norėčiau pasiimti su savimi šį patiekalą, ar galite jį supakuoti?** - Я хотел бы забрать это блюдо с собой, можете упаковать?
**Ar galima apmokėti kortele?** - Можно ли оплатить картой?
**Ar galiu palikti arbatpinigių kortele?** - Могу ли я оставить чаевые картой?
