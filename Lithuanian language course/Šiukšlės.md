# Šiukšlės

uošvė - теща
MOKESTIS - налог
DARBO UŽMOKESTIS - зарплата 
TAIKA - мир
POKALBIS - диалог, разговор
ne visai - не совсем
patenkintas - удовлетворен
IŠSILAVINIMAS - образование
NUO MEILĖS IKI NEAPYKANTOS - VIENAS ŽINGSNIS
ŠAKNYS - корнеплоды 
VALSTIETIS - крестьянин 
kaimiškas - деревенский 
LAIMĖ - счастье
VANDENYNAI - океаны
dykumas - пустыня
kęsti - kenčia - kentė - страдать
spalvinti - spalvina - spalvino - раскрасить
piešti - piešia - piešė - рисовать 

pavogti - украсть
pripratau - я привык
suklydau - ошибся
pataisyti - исправить
sugenda - ломается
sulūžta - сломан
nebepajėgia - больше не способен
plokščiosios replės - плоскогубцы
adata - иголка
siūlai - нитки
Sveikas protas - здравый смысл
Aš jais netikėjau - я им не поверил
Aš jais netikiu -
Reikalingas prekes radau pati -

laikyti - laiko - laikė - держать ( сдавать экзамен, испытание)
dieninės studijos - очное обучение
neakivaizdinės - заочный
VAIZDAS - вид, внешность
aš mokausi
tu mokaisi
jis mokosi
mes mokomės
jūs mokotės
jie mokosi
as mokausi lietuviu kalbos
šypsotis - šypsosi - šypsojosi - улыбаться
ŠYPSENA - улыбка
PUSIAUDIENIS -  полдень
PUSDIENIS - половина дня
drausti - draudžia - draudė - запрещать
leisti - разрешать
geras pažymys - хорошая отметка
geri pažymiai - хорошие отметки
TAIKOMOJI MATEMATIKA - прикладная математика
TAIKOMOJI DAILĖ - прикладное искусство
taikyti - taiko - taikė - применять
PROGRAMAVIMAS - программирование
EKONOMIKA IR VADYBA - экономика и менеджмент (управление)
laikyti teises - сдавать на права
tarti - taria - tarė - молвить
DELNAS - ладонь
pagaliau - наконец

kas dedasi? - что происходит?
itin - особенно
ypač - особенно
vietoje - на месте
išsinešimui - на вынос

GAMYKLA - завод
ĮMONĖ - предприятие
FABRIKAS - фабрика
VĮ - valstybinė įmonė -государственное предприятие
ĮSTAIGA - гос.учереждение
ISTATAI - уставы
VšĮ - viešoji įstaiga -
BENDROVĖ - общество
MB -
AB - akcinė bendrovė - акционерное общество
UAB - uždaroji akcinė bendrovė - закрытое акционерное общество
IĮ - individuali įmonė - индивидуальное предприятие
individuali veikla - индивидуальная деятельность
verslo liudijimas - ремесленое свидетельство
VERSLAS - ремесло
VALDYTOJAS -МЕНЕДЖЕР
VADOVAS - руководитель
VADAS - вождь
PIRMININKAS - председатель
SARGAS - сторож
SLAUGYTOJAS, A - мед.сестра
KIRPĖJAS, A - парикмахер
KEPĖJAS, A - пекарь
SIUVĖJAS - шея
Ką sakote? - что говорите?
pradėti - pradeda - pradėjo - начинать
SUSITIKIMAS - встреча
buvo dviejų prezidentų - было два президента
pareiti - возвратиться/вернуться туда, где был
PRAEITIS - прошлое
ATEITIS - будущее
DABARTIS - настоящее
persodino - пересадили
SMUIKAS - скрипка
įdėti - класть
uždėti - положить
TEISININKAS - юрист
šauti - šauna - šovė - стрелять
pulti - puola - puolė - нападать
KASTUVAS - лопата
ŽAIZDA - рана
pulti - нападать

SKYSTIS - жидкость
užsikrėsti - заразиться
ĮSITIKINIMAS -  убеждение
ANTIKŪNIAI - антитела
ne juokai - не шутка
nuo gerklės skausmo - от боли в горле
SUAUGĘS - взрослый
atsargus, i - осторожный
jaustis - чувствуете себя
žaloti - žaloja - žalojo - вредить
vengti - vengia - vengė - избегать
kenksmingas, a - вредный
PAVOJUS - опасность
PASLAPTIS - тайна
VIENUOLYNAS -монастырь
mokausi pati - учу сама

10 tablečių
purkšti - purškia - purškė - брызгать
PURKŠTUKAS - разбрызгиватель
spausti - spaudžia -  spaudė - нажимать
iki paskutinio kraujo lašo - до последней капли крови
kraujuotas, ta - кровавый
kraujuoti - kraujuoja - kraujavo - кровить/течет кровь
nukraujuoti - истекать кровью
KRAUJO SPAUDIMAS - кровянное давление
išsivemti - вытошнить из себя
VĖMIMAS - рвота
VIDURIAVIMAS - понос
VIDUS - внутрь
VIDURYS - середина
užkrėsti - užkrečia - užkrėtė - заражать
užsikrėsti - заразаться kuo? чем gripu, covidu
UŽKREČIAMOSIOS LIGOS - заразные болезни
degti - dega - degė - гореть
degtinė be alaus, pinigai į kanalizaciją - водка без пива...
degtinė - водка
uždegti - зажечь
ŽVAKĖ - свеча
UŽUOLAIDA - штора
INKSTAI - почки
PLAUTIS - легкое

didelė akis - didelės akies - didelę akį
didelis dantis - didelio danties - didelį dantį
du namai - dveji džinsai
dvi mergaitės - dvejos durys

ĮSTATYMAS - закон
prausti - prausia - prausė - умывать (одушевленного какого-то)
praustis - prausiasi - prausėsi - умываться самому
nusiprausti - nusiprausia - nusiprausė - умыться
ėsti - ėda - ėdė - жрать (есть для животных)
ĖDALAS - жрачка (негативный оттенок)
karščiuotis - горячиться
MANKŠTA - зарядка
SKIEPAI - прививки
PATARIMAS - совет
KAUKOLĖ - череп
KAUKĖ - маска
NĖŠČIOJI - беременная

raštuotas, a - узор
išorinė - снаружи
VIDUS - внутри
IŠORĖ - снаружи
išsamus, i - исчерпывающая, подробная
smulkus, i - подробная
semti - черпать
sudetis - состав
PŪKAS, ai - пух
PIENĖ - одуванчик
PLUNKSNA - перо
ANTIS - перо
KAPIŠONAS -
apgaubti - окутать,
plyšti
išmesti
iširo
teikti - teikia - teikė
LYGIS - уровень
pilietė - гражданка
išėjo iš darbo - уволится (уйти с работы)
išėjo iš proto - сойти с ума.
AMŽIUS - возраст, век.
paimti - брать
JUOKAS - смех
juoktis- juokiasi- juokėsi - смеяться
ATSITIKIMAS - проишествие
SKAUSMAS - боль

BADAS - голод
čiupti - čiumpa - čiupo - схватить
KRYPTIS - направление
LOBIS - клад
PLĖŠIKAS -пират
plėšti - грабить
slaptas, a - секрет
PASLAPTIS
spėti - spėja - spėjo - успевать
labai - labiau - labiausiai - больше
gerai  geriau  - geriausiaia - лучше
URVAS - нора
manyti - mano - manė - понимать
ŽĄSIS - гусь
trukdyti - мешать, отвлекать
kanda - кусают

NUOMONĖ - мнение
balta baltutėlė - белая-белая
siuvinėtas, a - с вышивкой
išeiginiai - выходная,на выход
ŠLEPETĖS - шлепанцы
nukainuoti - снизить цену
NUKAINAVIMAS - снижение цены
SKRYBĖLĖ - шляпа
GUMA - резина
kramtomoji guma - жевательная резинка
kramtyti - kramto - kramtė
guminis, ė - резиновый
guminiai batai - резиновые сапоги
LĖLININKĖ - куклочница, та, кто делает куклы
KALIOŠAS -галоши
ILGAAULIAI BATAI - высокие сапоги (с длинным галенищем ботинки)

Mano batai buvo du
vienas dingo - nerandu.
Aš su vienu bateliu
Niekur eiti negaliu
Dvi rankytės ieško bato.
Dvi akytės jį pamato.
Aš apsiausiu batelius - я обую ботиночки
Ir keliausiu pas vaikus

ruošti - подготовить
MUSELĖ - мушка
KAPAS - могила
KAPINĖS - кладбище
PAPUOŠALAS - украшения
VĖLĖ - поздно
dažyti - dažo - dažė - красить
DAŽAI - краски
klijuoti - клеять
kloti - выкладывать
TRINKELĖ - тротуарная плитка
kraustyti - krausto - kraustė - перевозка
griauti - griauna - griovė - разрушать
matuoti - matuoja - matavo - замеры
vėdinti - проветривать
ĮRENGIMAS - обустройство
dailus, i - красивый
SKYLĖ - дыра
GRĄŽTAS - сверлить
vesti - veda - vedė - вести
APKLOTAS  - UŽKLOTAS - накрыть
tempti - tempia - tempė - натянуть
MILTELIAI - порошковая
APLINKA - окружение
BŪRYS - отряд
užtenka - достаточно
tikrinti - tikrina - tikrino - проверять
patikrinti - проверить
nuolatos - постоянные
degti - dega - degė - гореть
ŽIBINTAS -  фонарь
PRIEKIS - перед
atsibodo - надоело
megzti - mezga - mezgė - вязать
SIŪLAI - нитки
VERPALAI - пряжа
vilnoniai siūlai - шерстяные нитки
striukas, a trumpas, a - короткий
leisti - leidžia - leido  - позволять, разрешать
PALEISTUVĖ - проститутка
VESTUVĖS - свадьба
APAČIA - внизу
APATINIS - комбинация (то, что жен. поддевают под платье)
glaustai kalbant - кратко говоря
LIEMENUKAS - лифчик

uostyti - uosto - uostė - нюхать
išties -  на самом деле
troškinti - тущеные
krimsti - kremta - krimto - грызть
KEPĖJAS - текарь
MEŠKERĖ - удочка
tikri - настоящий
su kaminu - с трубой
TROBA - изба
VALTIS - лодка
DŪMAS - ai - дым
DANGUS - небо
paupy - под рекой
gilus, i - глубокий
MINTIS - мысль
IŠMINTIS - мудрость
padėkite - помогите
šiukšlių dėžė - мусорное корзина
ŠIUKŠLINĖ - мусорка
rankų darbo kilimas - ручной работы ковер

GAUDYKLĖ - ловушка
BAIDYKLĖ - пугало
DRAMBLYS - слон
TRŪKUMAS - недостаток
SĄJUNGA - союз
TALPA - емкость
purvinas, a - грязный
dulkinas, a - пыльный
I RŪŠIS - первый сорт
rūšiuoti - rūšiuoja - rūšiavo - сортировать
MAISTO ATLIEKOS - пищевые остатки
VEIDAS - лицо
džiovinti - džiovina - džiovino - сушить
DŽIOVA - туберкулез
virti - verda - virė - варить

LENTA - доска
MALDA - молитва
MALDININKAS - поломник
stebuklas - чудо
AUŠRA - заря
likti - lieka - liko - остаться
GALAS - конец
GINKLAS - оружие
SARGAS - сторож
PRIEŠAS - враг
saugoti - saugo - saugojo - охранять
JUOSTA - лента
medinę kėdę - деревянный стул
iki vidurio nakties - до середины ночи
VIDURYS - середина
GRĖBLYS - грабли

skirtingi dalykai - разные вещи
įkvėpimo - вдохновение

vieno kambario butas - однокомнатная квартира
užmigti - užmiega - užmigo - засыпать
įmokėti - оплатить какой-то взнос
DUJOS - газ
MOKESTIS - плата (платить за что-то одно)
TURTAS - богатство
turtingas, a - богатый
knygų lentyna - книжная полка
vienvietis - одноместный
SARGAS - сторож
KIEMSARGIS - дворник
status - крутой
statūs laiptai - крутая лестницв
vienaukštis - одноэтажный
daugiaaukštis - многоэтажка

nusileidimas - приземление, посадка

PUŠIS - сосна
retas kartas - редкий случай
rodyti - rodo - rodė - показывает
šokinėti - šokinėja - šokinėjo - прыгать
išdykęs -usi - непаслушный
verkti - verkia - verkė - плакать
SUNKVEŽIMIS - грузовик, "тяжеловоз"
vežti - veža vežė
VEŽIMAS - воз
VEŽIMĖLIS - дет коляска
riedėti - rieda - riedėjo - катить
tranzuoti - tranzuoja - tranzavo - авто-стоп,
bet - tačiau - но, однако
be vargo - без труда
nors - хотя
gauti - gauna - gavo - получать
kol kas  - пока что
Jis kol kas neatejo - он пока что (ещё) не пришёл
ilgalaikis, ė -долгосрочный
trumpalaikis, ė - краткосрочный
Siaubingai - Ужасно

nervinti - ?
BUITIS - быт
užrakinti -  закрыть на ключ
JAUSMAS - чувство
virpėti - virpa - virpėjo - дрожать
drebėti - dreba - drebėjo - дрожать
smarkūs - жестокий
tuksenti - tuksena - tukseno - стучать
tikras, a - настоящий
šnekėti - šneka - šnekėjo - говорить
poryt - послезавтра
PAVARA - скорость (передача)
VIRŠUS - вверх
APAČIA - вниз
DRAUSMĖ - дисциплина

ATSITIKIMAS - авария
BLYKSNIS - вспышка
leidžiasi - допускается
leidesi - спускаться?
leistis žemyn - спускаться вниз
LAKŪNAS - пилот
GRIAUSTINIS - гром
gąsdinti - gąsdina - gąsdino - пугать
išsigąsti - išsigąsta - išsigando - испугались
ĮŽYMYBĖ - достопримечательностей ( пр.перевод известное)
pataikė - ударила
ŽAIBAS - молния
VAIRAS - руль
TEISĖS - права (водительские)
PARKAVIMAS - парковка
RAŠYTOJAS - писатель
atsistoti - встать
elektrinis, ė - электрический
elektroninis, ė - электронный
ATSAKOMYBĖ - ответственность
PATIRTIS - опыт
nėra patirties - нет опыта

DEGALAI - товливо
REIKALAI - дела
VARIKLIS - двигатель
varyti - varo - varė - гнать
varom - погнали
aš varau, tu varai
gerai pavarė - круто сделал
pilti - pila - pylė - заливать
užpilti - залить
pripilti - налить
įpilti - влить
SUSIRINKIMAS - собрание
SUSITIKIMAS - встреча
gesti - genda - gedo - портится
sugesti - sugenda - sugedo - испортилось
TAISYKLA - мастерская
PLOVYKLA - мойка
reikėti - reikia - reikėjo - нуждаться
STABDYS - тормоз
stabdyti - stabdo - stabdė - тормозить
veikti - veikia - veikė - действовать
VEIKSMAS - действие
VEIKSMAŽODIS - глагол
VALYTUVAS - дворник на автомобили
VAIRAS - руль
vairuoti - vairuoja - vairavo - рулить
VAIRUOTOJAS - водитель (рулитель)
keliauti - keliauja - keliavo - путешествовать
drausmingas, a - послушный, дисциплинированный
ELGESYS - поведение
PAVOJUS - опасность
pavojingas, a - опасный
dengti - dengia - dengė - покрывать
DANGTELIS - крышка
daugmaž - более-менее
patepti - намазать
užtepti - замазать
GRĄŽTAS - сверло

gimti-gimsta-gime - рождаться
tėvynė - родина
VAIKYSTĖ - детство
JAUNYSTĖ - юнсость
SENATVĖ - старость
PAAUGLYSTĖ - подростковый возраст
paauglys, paauglė - подросток
sauaguęs - взрослый
TAUTYBĖ - НАЦИОНАЛЬНОСТЬ
tauta - нация
taryba -  совет
GYTENTOJAS, A - жителей
GIMINAITIS, - родственник
PRIEMIESTIS - пригород
nereikialingas - ненужный

leisti - leidžia - leido - пускать, пропускать
galas - конец
vagono gale - в конце вагона
tiesė - прямая
atsuktuvas - отвертка
lėkti - lekia - lėkė - лететь (мчаться, спешить)
ūsas - ус
kalva - холм
toli->toluma->tolumon - в даль
į darbą - darban
be grąžos - без сдачи

Labai tikiuosi - очень надеюсь
Palikime arbatpinigių! - оставим чаевые
aptarnauti - обслужить
Galiu įdėti į dėžutę. - могу положить в коробку
valgiaraštis - меню, список блюд
renginys - событие
straipsnis - статья
prekė - товар
kulverstukas - чебурашка
vėjuota - ветренно
uraganas - ураган
pavyko pasivaikščioti - удалось прогуляться
čiuožykla - горка
mokėti, moka, mokėjo čiuožinėti - уметь, умеет, умел кататься на коньках
pusantros valandos iki Akropolio - полтора часа до Акрополя
ilgas atstumas - длинная дистанция
patalpa - помещение
saulėta diena - солнечный день

viešbutis - отель
į viešbutį - в отель
durys - дверь
atidaryk duris - открой дверь
grįžo - вернулся
mes grįžome - мы вернулись
jūs grįžote - вы вернулись
jis, ji, jie, jos, grįžo - вернулся, вернулась
aš grįžau - я вернулся
tu grįžai - ты вернулся
rašė - написал
aš rašiau
tu rašei
jis, ji, ji, jos rašė
mes rašėme
jūs rašėte
valgyti
valgyk
valgykite
išjunk
kai yra nuotaika - когда есть настроение
sunku - сложно
nesmagu - не весело
kai kviečiu kažką į svečius - когда я кого-то приглашаю
aš nesutaisiau pats/pati - я сам не чинил
pakeisti, pakeičia, pakeitė filtrą -  заменить фильтр
visai dienai
patikrinti, patikrina, patikrino - проверять
sužinoti, sužino, sužinojo informaciją - выяснить, узнать
kada nors - когда-то
užtrunka - занимает время

sveikatos ir laimės - здоровья и счастью
pinigų ir meilės - деньги и любви
greitai praėjo - прошло быстро
įprastas - простой
viską pomotity - все посмотреть
neprisumino - не помню
Tiksliai nepamenu - не помню точно

man šiandien gera nuotaika/aš esu geros nuotaikos - в хорошем настроении
smagu - весело
patikrinty - проверить

uošvė, anyta - свекровь
pūga - метель
įmanoma - возможно
mugė - ярмарка
bendradarbiais - сотрудники
susitikome
pirtis - сауна
aš praėjau pro senamiestį - я прошел через старый город
darbingas - workable
turiningas - full of events
smagus savaitgalis - хороших выходных
eksponatai
buvo vėjuota ir lietinga - было ветрено и дождливо
puga - storm
kiek laiko būsite? - how long you will be
smagių atostogų - fun vacation

užsidėti, užsideda, užsidėjo akinius, kepurę, kaklaraišt
apsiauti, apsiauna, apsiavė - надеть
patogūs batai
šviesūs batai
vyriški batai
smagi suknele - милое платье
suknelė - платье
vilnoniai - шерстяной
vienspalviai - одноцветный, монохромный
patogios pirštinės
patogūs batai
malonu jus vėl matyti - приятно видеть вас снова
midijos - mussels - мидии
įdomus - интересный
įprastas - обычный
gaminti, gamina, gamino - производить
sėkmingas, -a

mano savaigolis buvo neblogas. neko naulu. žurijau seriala. buvo plana pailseti nuo preito savaitgalio.
ar pavyko? - это сработало?
aš ijeo į parka - я пошел в парк
labai gaila - очень жаль
kiek laiko tai užtruks? - сколько времени это займет?
kaip greitai - как быстро
paimti, paima, paėmė - take some one from some place
nuvežti, nuveža, nuvežė - bring some one to some place
laukti, laukia, laukė - wait for
mano savaitgalis buvo puikus. Daug vaikshoja meste
aš mėgavausi, mes mėgavomės - enjoy
aš mėgaujuosi - im enjoing
perskaičiau - finish reading
labai trumpai - very short

šis-šitas
ši-šita
šie-šitie
šios-šitos
aš tikiuosi - я надеюсь
pagal naują receptą - по новому рецепту
išbandyti naują receptą - попробовать новый рецепт
kiekvieną sekmadienį - каждое воскресенье
pamoka - урок
užsiėmimas - занятие
lėkštė - тарелка
vėliavos pakėlimo ceremonija - церемония поднятия флага
memorialo atidarymas - открытие мемориала
turiningas - содержание
turėjau turiningą savaitgalį у меня были насыщенные выходные
buvo turiningas savaitgalis - были насыщенные выходные
klientams nerūpi - клиенты не заботятся
aš pavargau - я устал
įprastas - обычный
sirgti, serga, sirgo - болеть, болеет, болел
savaitgalis prasidėjo - выходные начались
prasidėti, prasideda, prasidėjo - начинать
tingus - ленивый
tegu jis - пусть
išberti, išberia, išbėrė ką? (veidą, kūną) - пятна на лице, теле
praryti, praryja, prarijo - проглотить
kvėpuoti, kvėpuoja, kvėpavo - дышать
pavojinga - опасно
vartoti, vartoja, vartojo - употреблять
kodėl negalite vartoti..? - почему нельзя употреблять
aš stengiuosi - я пытаюсь
rūkyti, rūko, rūkė - курить
būti, būna, buvo gryname ore - быть на свежем воздухе
užtenka - достаточно, хватит
pavyksta - удается, получается
daug laiko - долгое время
įmanoma - возможно
manyti, mano, manė
liūdinti, liūdina, liūdino
mane liūdina - заставляет меня грустить
būgnai - барабаны
nebiagiau - не закончил
baigti, baigia, baigė muzikos mokyklą - окончить муз.школу
užkandžiauti, užkandžiauja, užkandžiavo - take snak
vietoj - вместо
nutukę žmonės - полные люди
spręsti, sprendžia, sprendė - решать
turtingas - богатый
kiekvienas žmogus -каджый человек
turėtų - должен
greitas maistas - фаст-фуд
aš laikausi dietos
laikiausi dietos
priaugti, priauga, priaugo svorio
numesti, numeta, numetė svorio
būti blogos/ geros nuotaikos
pakankamai - достаточно
man trūksta miego - мне не хватает сна
(pa)snausti, snaudžia, snaudė - дремать
išskyrus - кроме
aš keliuosi/atsikeliu - поднимаюсь, просыпаюсь
patirti, patiria, patyrė - испытывать
kovoti, kovoja, kovojo - бороться
atsispirti, atsispiria, atsispyrė - сопротивляться
keisti, keičia, keitė - менять
atidaryti, atidaro, atidarė sezoną - отрыть сезон
mus užklupo lietus - попали по дождь
su nuotykiais - с приключениями

amatininkė - мастерица
papuošalai, aksesuarai - украшения, аксессуары
todėl kad - потому что
geriausiai apmokamas - самый высокооплачиваемый
gerai apmokamas - хорошо оплачиваемая
aš manau, kad - я думаю, что
daug atsakomybių - много обязанностей
didelė atsakomybė - большая ответственность
labai atsakingas darbas - очень ответственная работа
prestižinės profesijos - престижные профессии
reikia bendrauti su žmonėmis - нужно общаться с людьми
kalnakasys - шахтер
kantrybė - терпение
pagaliau - окончательно
naudingas - полезный
vaikystėje - в детстве
tapti, tampa, tapo kuo? - стать
nekilnojamo turto agentas,-ė - риэлтор
reikia daug kantrybės - нужно много терпения
dabartinis darbas - текущая работа
laimei, buvo klaida - повезло, была ошибка
paslauga, padengta draudimo
atsispausdina, atsispausdino
paukščių čiulbėjimą - щебетание птиц
mes turime atsakingą asmenį - у нас есть ответственный
persikraustyti, persikrausto, persikraustė - переехать
kraustytis, kraustosi, kraustėsi
aš kraustausi į naujus namus
keisti, keičia, keitė padangas - менять шины
statyti, stato, statė - строить
lipdyti, lipdo, lipdė sniego senį - лепить снеговика
uostyti, uosto, uostė - нюхать
sniego gniūžtės - снежки
įgūdis - умение
elektroniniai prietaisai - электронные устройства
sutaupyti, sutaupo, sutaupė pinigų - сберечь деньги.
lazdelės - палочки
vairuoti, vairuoja, vairavo - водить
šokinėti, šokinėja, šokinėjo - прыгать
giminės/giminaičiai - родственники
vaišės - угощение
vaišintis, vaišinasi, vaišinosi  - угощайся
vaišinti, vaišina, vaišino - угощать
storas - толстый
lieknas - стройный
riebus - жирный
liesas, liesa - тощий
garsus - громкий
nuobodulis - скука
keistas - странный
kvailas - глупый
kvailys - глупец
priminimas - напоминание
atsiliepimas - отзыв
duomenis -  данные
patalpas - помещение
man atrodo, kad... - мне кажется, что...
miegas po atviru dangumi - спать на свежем воздухе
paskola - заем
Rink taškus, keisk į prizus! - Накапливай баллы, обменивайте их на призы!
rimtai? - серьезно?
Sklypai už Vilniaus - участки за Вильнюсом
ištrūk iš meisto - выйди из города
atsitiktinai - случайно
vis delto - однако, все-таки
tačiau - но, однако
dėl to - потому что
tarsi - будто
šukuotis -  причесываться
šukuti - расчесывать
pavalgęs - сытый, поевший
vaišės - угощения, пир
šypsena - улыбка
pypke - трубка для курения
apkūnus - полный, упитаный
plikas - лысый

pagalvė - подушка
antklodė - одеяло
paklodė - простыня
užuolaidos - шторы, занавески
rankšluostis - полотенце
orkaitė - духовка
dėžė - ящик
rūšiuoti - cортировать

kabėti - висеть
kabinti - вешать
apkabinti - обнять
skalbti - стирать

šluota - метла
šluoti - мести

kibiras - ведро

viešosios tvarkos - общественный порядок
purvinas - грязный
ramus - спокойный
atskiras  - отдельный
tvankus - душный
kietas - твердый, крепкий
plytinis - кирпичный
molinis - глининый

viduje - внутри
vietoje - наместе
lauke - снаружи
tvora - забор
vartai - ворота
varteliai - калитка
krūmas - куст
suolas - скамейка
veja - газон

nakvoti - провести ночь

taupyti - наэкономить, скопить
persikelti - переехать
persikraustyti - переселиться

stiklinė - стакан, стяклянный
stiklainis - банка
viešnagė - прибывание вне дома, в гостях
viešnia - гостья

man teko -  мне пришлось
man norėjosi, kad būtų buvę tyliau - мне бы хотелось, чтобы было тише
svajoti, dvajoja, svajojo - мечтать

suaugusieji - взрослые
pakloti, pakloja, paklojo - заправлять
paklota lova - заправленная кровать
bala, balos - лужа
išlieti, išlieja, išliejo - разлить (нечаянно)
išlietos vanduo
batu raišteliai, batraiščiai - штурок
užrišti - завязать
atrišti - развязать
voras - паук
voratinklis - паутина
nuvysti, nuvysta, nuvyto - завять
nuvytusios gelės - завядшие цветы
išmėtyti -  разбросать
trupiniai - крошки
trupinti, trypina, trupino - крошить
ant stalo pritrupinta - на столе накрошено
patirk skirtuma - испытай разницу

as pasiduodu - я сдаюсь
paaiškinti - объяснить
pasirodyti - появиться
neprireiks - не понадобится

paskutinis ispejimas sumoketi bauda - последнее предупреждение об уплате штрафа

šilauogės - (šilas + uogos) черника
kaulas -kaulais - кос
rėkti, rekia, rėkė -  кричать
