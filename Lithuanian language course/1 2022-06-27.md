# #1 2022-06-27

Material: 28.pdf 29.pdf
**Mano  vardas yra Yauheni** - мое имя есть Евгений
**Koks tavo vardas?** - как твое имя
**Koks jusu vardas?** -  тоже  Ваше имя (кокс юсу вардас)?
**Kokia tavo pavarde?** - .. фамилия (ж.р) кокя таво паварде.

**Is kur tu esi?** - из где ты есть? Откуда ты? (иш кур ту эси)
**Is kur jus esate?** - откуда Вы (иш кур юс Эсатя)
**As esu is Lietuvos** - я есть из Литвы (аш эсу иш Летувос)
As esu is Baltorusijos is Minsko -
Baltarusija,   is Baltarusijos
Minskas, is Minsko

**Kur tu eini?** - Куда ты идешь? (Кур ту эйни?)

**Sveiki** - здравствуйте (мн.число, или на Вы)
**Sveikas** - ... мужчине, к которому на ты.
**Sveika** - ... женщинe, к которой на ты.

Labas rytas - доброе утро
Laba diena - добрый день
Labas vakaras - добрый вечер
(Laba vakara - доброго вечера)

Labas - привет неформальный
Labukas - приветики

Viso gero - всего хорошего
Iki pasimatimo - До встречи (Ики пасимАтимо).
iki - тоже самое что и iki pasimatimo, но сокращенное
sudie - (устарелое) с Богом.
ate - пока (атия)
labanaktis - спокойной ночи. (лабАнактис)

atsiprasau - извиняюсь. (аципрашау)
nieko tokio - ничего страшного. (Ниэко токё).

taip - да
ne - нет (ния)

Geros dienos - хорошего дня (герос дьенос)
as nesuprantu - я не понимаю
Prasom pakartoti - прошу повторить (прашом пакартоти)
Kaip sekasi? - как дела (кэйп сЯкаси)
Kaip gyveni? - как живешь (длительное время не видел). (кэйп гивяни).
Labei gerai - оч хорошо
Neblogai - неплохо

As esu - Я есть
Tu esi- ты есть
Jis  yra- он есть (ис Ира)
Ji yra- она есть (и ира)
Mes esame- мы есть  (мяc эсаме)
Jus esate- Вы есть (юс Эсатя)
Jie yra- они муж. есть (е Ира)
jos yra- они жен. есть (ёс ира)

Kas - кто,что (кас)
Kur - где, куда (кур)
Kada - когда (када)
Kiek - сколько (кек)
Koks - какой (кокс)
Kokia - какая (кокя)
Kokie - какие (коке)
Kodel - почему (кодел)
Kaip - как (кейп)

Buti, yra, buvo, bus - быть, есть, было, будет

metai - год (мЯтэй)
menesiai -месяцев (мЕнясей)
vienas menuo - один месяц (венас мяно)
savaite - неделя (савайтиэ)
trys savaites - три недели

vienas
du
trys
keturi
penki
sesi (шеши)
septyni
astuoni (аштуОни)
devyni (дЭвини)
desimt (дэщемт)

abecele - алфавит (абецеле)
j - jot - ю.ё

Labai malonu - очень приятно
Malonu susipazinti - приятно познакомиться (малону сусипажинти)

labas
kur ti eini
koks tavo vardas?
Kaip geveni? - как живешь?

Kavos su pienu

ДЗ: спросить как дела, представиться 10 раз.
