# Глаголы

**atostogauti, atostogauja, atostogavo – to have vacation, to be on holiday**
Aš atostogavau, tu atostogavai, jis-ji-jie-jos atostogavo; Mes atostogavome, jūs atostogavote
Aš atostogauju, tu atostogauji, jis-ji-jie-jos atostogauja; Mes atostogaujame, jūs atostogajate
Aš atostogausiu, tu atostogausi, jis-ji-jie-jos atostogaus; Mes atostogausime, jūs atostogausite

**daryti, daro, darė – to do, to make**
Aš dariau, tu darei, jis-ji-jie-jos darė; Mes darėme, jūs darėte
Aš darau, tu darai, jis-ji-jie-jos daro; Mes darome, jūs darote
Aš darysiu, tu darysi, jis-ji-jie-jos darys; Mes darysime; jūs darysite

**veikti, veikia, veikė – to do**
Aš veikiau, tu veikai, jis-ji-jie-jos veikė; Mes veikėme, jūs veikėte
Aš veikiu, tu veiki, jis-ji-jie-jos veikia; Mes veikiame, jūs veikiate
Aš veiksiu, tu veiksi, jis-ji-jie-jos veiks; Mes veiksime, jūs veiksite

**keliauti, keliauja, keliavo – to travel**
Aš keliavau, tu keliavai, jis-ji-jie-jos keliavo; Mes keliavome, jūs keliavote
Aš keliauju, tu keliauji, jis-ji-jie-jos keliauja; Mes keliaujame, jūs keliaujate
Aš keliausiu, tu keliausi, jis-ji-jie-jos keliaus; Mes keliausime, jūs keliausite

**važiuoti, važiuoja, važiavo – to go by vehicle (ехать)**
Aš važiavau, tu važiavai. jis-ji-jie-jos važiavo; Mes važiavome, jūs važiavote
Aš važiuoju, tu važiuoji, jis-ji-jie-jos važiuoja; Mes važiuojame, jūs važiuojate
Aš važiuosiu, tu važiuosi, jis-ji-jie-jos važiuos; Mes važiuosime, jūs važiuosite

**atvykti, atvyksta, atvyko – to arrive**
Aš atvykau, tu atvykai, jis-ji-jie-jos atvyko; Mes atvykome, jūs atvykote
Aš atvykstu, tu atvyksti, jis-ji-jie-jos atvyksta; Mes atvykstame, jūs atvykstate
Aš atvyksiu, tu atvyksi, jis-ji-jie-jos atvyks; Mes atvyksime, jūs atvyksite

**išvykti, išvyksta, išvyko – to depart, to leave (отправляться)**
Aš išvykau, tu išvykai, jis-ji-jie-jos išvyko; Mes išvykome, jūs išvykote
Aš išvykstu, tu išvyksti, jis-ji-jie-jos išvyksta; Mes išvykstame, jūs išvystate
Aš išvyksiu, tu išvyksi, jis-ji-jie-jos išvyks; Mes išvyksime, jūs išvyksite

**skristi, skrenda, skrido – to fly, to go by plane**
Aš skridau, tu skridai, jis-ji-jie-jos skrido; Mes skridome, jūs skrisote
Aš skrendu, tu skrendi, jis-ji-jie-jos skrenda; Mes skrendame, jūs skrendate
Aš skrisiu, tu skrisi, jis-ji-jie-jos skris; Mes skrisime, jūs skrisite

**plaukti, plaukia, plaukė – to swim**
Aš plaukiau, tu plaukei, jis-ji-jie-jos plaukė; Mes plaukėme, jūs plaukėte
Aš plaukiu, tu plauki, jis-ji-jie-jos plaukia; Mes plaukiame, jūs plaukiate
Aš plauksiu, tu plauksi, jis-ji-jie-jos plauks; Mes plauksime, jūs plauksite

**grįžti, grįžta, grįžo – to return, to come back**
Aš grįžau, tu grįžai, jis-ji-jie-jos grįžo; Mes grįžome, jūs grįžote
Aš grįžtu, tu grįžti, jis-ji-jie-jos grįžta; Mes grįžtame, jūs grįžtate
Aš grįžsiu, tu grįžsi, jis-ji-jie-jos grįžs; Mes grįžsime, jūs grižsite

**mokėti, moka, mokėjo –  to know how to do smth**
Aš mokėjau, tu mokėjai,  jis-ji-jie-jos mokėjo; Mes mokėjome, jūs mokėjote
Aš moku, tu moki, jis-ji-jie-jos moka; Mes mokame, jūs mokate
Aš mokėsiu, tu mokėsi, jis-ji-jie-jos mokės; Mes mokėsime, jūs mokėsite

**patikti, patinka, patiko – to like**
Aš patikau, tu patikai, jis-ji-jie-jos patiko; Mes patikome, jūs patikote
Aš patinku, tu patinki, jis-ji-jie-jos patinka; Mes patinkame, jūs patinkate
Aš patiksiu, tu patiksi, jis-ji-jie-jos patinks; Mes patinksime, jūs patinksite

**mėgti, mėgsta, mėgo – to like**
Aš mėgau, mėgai, jis-ji-jie-jos mėgo; Mes mėgome, jūs mėgote
Aš mėgstu, tu mėgsti, jis-ji-jie-jos mėgsta; Mes mėgstame, jūs mėgstate
Aš mėgsiu, tu mėgsi, jis-ji-jie-jos mėgs; Mes mėgsime, jūs mėgsite

**klausyti, klauso, klausė – to listen**
Aš klausiau, tu klausei, jis-ji-jie-jos klausė; Mes klausėme, jūs klausėte
Aš klausau, tu klausai, jis-ji-jie-jos klauso; Mes klausome, jūs klausote
Aš klausysiu, tu klausysi, jis-ji-jie-jos klausys; Mes klausysime, jūs klausysite

**skaityti, skaito, skaitė – to read**
Aš skaitiau, tu skaitei, jis-ji-jie-jos skaitė; Mes skaitėme, jūs skaitėte
Aš skaitau, tu skaitai, jis-ji-jie-jos skaito; Mes skaitome, jūs skaitote
Aš skaitysiu, tu skaitysi, jis-ji-jie-jos skaitys; Mes skaitysime, jūs skaitysite
