# Apie Tailando

Mano tekstas:
Laba diena!
2 savaites atostogavau Tailande, Phuketo saloje.
Tai mano trečioji kelionė į šią šalį, pirmoji buvo 2009 m.
Deja, didžiąją atostogų dalį sirgau, o po atostogų taip pat susirgau.
Tačiau tai nesugadino bendro kelionės įspūdžio, vis tiek labai patiko,
daug maudėmės jūroje, nardėme, deginomės, valgėme šviežius ir skanius vaisius bei jūros gėrybes,
važinėjome dviračiu, kasdien ėjome į tailandietišką masažą
Ką pasiimti į Tailandą:
• būtinai reikia turėti maudymosi kostiumėlį, kremą nuo saulės ir galvos apdangalą.
• dezinfekuojančių priemonių, vaistų nuo žarnyno infekcijų, peršalimo ir galvos skausmo,
• repelentų nuo uodų, nes gresia dengė karštligė.
• draudimą, be jo geriau niekur nevažiuoti
Ir gerą nuotaiką

+++++++++
Kai pavargsiu – ant širdies
tyliai, švelniai man uždėk
vasaros akių giedrumą,
tėviškės malonų dūmą,
šieno kvapą. Man uždėk

vieškelius su žmonėmis,
nubarstytus gėlėmis,
vaiko man dienas uždėk
su mažų langų žvaigžde.

Skarą prie laukų priglausk,
žemės veidą man atspausk,
ant širdies –
žemės tylą man uždėk.

O jei nieko neturėsi,
tai uždėk minčių man šviesą,
ligi skausmo užsimerk
ir akis tu man priverk
