# mtools

### Delete file in the image:

```
mdel -i usb-rtm-1.0.0.0.yauheni.img ::ifs_imx8qm_alphabot.bin
```

### Show directory content in the image (do  \`dir\`):

```
mdir -i usb-rtm-1.0.0.0.yauheni.img
```

### Copy file into the image:

```
mcopy ../qnx-7.1-bsp/rtm/rtm-ifs-1.0.0.0.yauheni.bin -i usb-rtm-1.0.0.0.yauheni.img ::
```
