# tcpdump

### Does someone ping your system?:

```
sudo tcpdump -i wlan0 icmp and icmp[icmptype]=icmp-echo
```
Original article: [How to check if someone ping your system using tcpdump?](https://taufanlubis.wordpress.com/2017/06/30/how-to-check-if-someone-ping-your-system-using-tcpdump/)
