# imagemagick

### How to check exif metadata:

```
yauheni@pc:~/Downloads$ identify -format '%[EXIF:*]' IMG20230723150421.jpg
```

### How to remove exif metadata from the photo:

```
yauheni@pc:~/Downloads$ mogrify -strip IMG20230723150421.jpg
```
