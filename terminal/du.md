# du

* size of directories

```
yauheni@pc:~$ sudo du -skh /*
```

* size of directories excluding one

```
yauheni@pc:~$ sudo du -skh /* --exclude=/mnt
```
