# tmux

### Make the sizes of panes equal:

```
<c-b>,<alt+1> - for vertical
<c-b>,<alt+2> - for horizontal
```

![[terminal/_resources/tmux.resources/image.png]]
