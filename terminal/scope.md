# scope

### How to use:

```
yaukarat@3c22fb65f66a % find . -name "*.c" -o -name "*.cpp" -o -name "*.h" -o -name "*.hpp" > cscope.files
yaukarat@3c22fb65f66a % cscope -q -R -b -i cscope.files
yaukarat@3c22fb65f66a % cscope -d
```

Links:

* [How to install and use cscope](https://codeyarns.com/tech/2017-08-24-how-to-use-vim-with-cscope.html#gsc.tab=0)
