---
tags: ["#binutils"]
---
# objdump

### \*.so are needed:

```
yauheni@pc:/opt/alert/toolchains/qnx-sdp/7.1.0$ objdump -x ./target/qnx7/aarch64le/usr/lib/libmenu.so | grep NEEDED
  NEEDED              libc.so.5
  NEEDED              libgcc_s.so.1
```
headers:
```
yaukarat@3c22fb65f66a production % objdump -f leo_acu.X.production.elf

leo_acu.X.production.elf: file format elf32-mips
architecture: mipsel
start address: 0x9d000020
```

```
yaukarat@3c22fb65f66a production % objdump -p leo_acu.X.production.elf

leo_acu.X.production.elf: file format elf32-mips

Program Header:
    LOAD off    0x00010000 vaddr 0x80000000 paddr 0x80000000 align 2**16
        filesz 0x000000c8 memsz 0x00004448 flags rw-
    LOAD off    0x00014448 vaddr 0x80004448 paddr 0x80004448 align 2**16
        filesz 0x00000000 memsz 0x00000000 flags rw-
    LOAD off    0x00014448 vaddr 0x80004448 paddr 0x80004448 align 2**16
        filesz 0x00048bac memsz 0x00048bac flags rw-
    LOAD off    0x00060020 vaddr 0x9d000020 paddr 0x9d000020 align 2**16
        filesz 0x000001b4 memsz 0x000001b4 flags r-x
    LOAD off    0x000604a0 vaddr 0x9d0004a0 paddr 0x9d0004a0 align 2**16
        filesz 0x00003b60 memsz 0x00003b60 flags r-x
    LOAD off    0x00064000 vaddr 0x9d004000 paddr 0x9d004000 align 2**16
        filesz 0x00000010 memsz 0x00000010 flags r-x
    LOAD off    0x00064010 vaddr 0x9d004010 paddr 0x9d004010 align 2**16
        filesz 0x000000f0 memsz 0x000000f0 flags r-x
    LOAD off    0x00064100 vaddr 0x9d004100 paddr 0x9d004100 align 2**16
        filesz 0x00000010 memsz 0x00000010 flags r-x
    LOAD off    0x00064110 vaddr 0x9d004110 paddr 0x9d004110 align 2**16
        filesz 0x00000070 memsz 0x00000070 flags r-x
    LOAD off    0x00064180 vaddr 0x9d004180 paddr 0x9d004180 align 2**16
        filesz 0x00000010 memsz 0x00000010 flags r-x
    LOAD off    0x00064190 vaddr 0x9d004190 paddr 0x9d004190 align 2**16
        filesz 0x00000070 memsz 0x00000070 flags r-x
    LOAD off    0x00064200 vaddr 0x9d004200 paddr 0x9d004200 align 2**16
        filesz 0x000018e0 memsz 0x000018e0 flags r-x
    LOAD off    0x00065ae0 vaddr 0x9d005ae0 paddr 0x9d005ae0 align 2**16
        filesz 0x00087a12 memsz 0x00087a12 flags r-x
    LOAD off    0x000f4450 vaddr 0xa0004450 paddr 0xa0004450 align 2**16
        filesz 0x00000000 memsz 0x00000480 flags rw-
    LOAD off    0x000f0540 vaddr 0xbf810540 paddr 0xbf810540 align 2**16
        filesz 0x00000358 memsz 0x00000358 flags rw-

Dynamic Section:
```
