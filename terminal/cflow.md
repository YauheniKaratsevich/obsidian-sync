# cflow

Link: <https://www.gnu.org/software/cflow/manual/cflow.html>

### List of functions of main in the file:

```
$ cflow ./src/app/common/main.c
main() <int main (void) at ./src/app/common/main.c:173>:
    prvMiscInitialization() <void prvMiscInitialization (void) at ./src/app/common/main.c:231>:
        xLoggingTaskInitialize()
        SYS_Initialize()
        SYS_Tasks()
    vInitResetReason()
```

### List of all functions in the file:

```
$ cflow --no-main ./src/app/common/main.c
```
