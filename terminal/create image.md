# create image

### Create a file:

```
dd if=/dev/zero of=file.my bs=2M count=1
```

### Bind file to loop device (not need if just format is needed):

```
sudo losetup -f file.my # -f means find free loop device
```

### Find your loop device:

```
sudo losetup -a | grep file.my
/dev/loop26: []: (/home/yauheni/tmp/img/file.my)
```

### Format loop device which is bind with file:

```
mkfs.vfat file.my # or mkfs.vfat /dev/loop26
```

### Remove loop device:

```
sudo losetup -d /dev/loop26
```
