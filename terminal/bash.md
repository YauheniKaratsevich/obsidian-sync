# bash

### Docs:

* [Bash Reference Manual](https://www.gnu.org/software/bash/manual/bash.html)
* [Advanced Bash-Scripting Guide (RU)](https://www.opennet.ru/docs/RUS/bash_scripting_guide/)
* [Advanced Bash-Scripting Guide (EN)](https://tldp.org/LDP/abs/html/)
* [Bash Cookbook (pdf)](https://terrorgum.com/tfox/books/bashcookbook.pdf)

### How to find info about bash built-in commands:

```
# help test
# help echo
```

### How to delete all file but few:

```
yauheni@yauheni-pc:~/edu/emb_linux/buildroot$ rm -rf -v !("version.txt"|"my_config"|".my_config")
```

Links:

* [bashunit (unit testing framework for bash)](https://bashunit.typeddevs.com/)
