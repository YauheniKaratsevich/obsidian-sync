# git

### Don't forget to do asap:

```
git config --global user.name "Your Name"
git config --global user.email "youremail@yourdomain.com"
git config --global core.editor vim
git config --global diff.tool vimdiff
git config --global merge.tool vimdiff
```

### How to find common ancestor (общий предок):

```
git merge-base master integration/EMBD-6294-alphabot-2-0
```

### How to redo hard reset:

```
git reflog        # find when it happend, assume at HEAD@{1}
git reset HEAD@{1}
```

### How to check branches and their tracking branches:

```
git branch -vv
```

### How to change tracking branch:

```
git branch embd-6953-rtm-master-install-script --set-upstream-to origin/mainline
```

### Remove files from stage:

```
git restore --staged <file>
```

### How to unstage files from index:

```
git reset
or
git restore --staged .
```

### Stash with text message:

```
git stash push -m "my_stash_name"
```

### Show and apply stash:

```
git stash list
stash@{0}: WIP on SDCOSMOS-68528: 1675ebd SDCOSMOS-68083: Clear FPLMN list and LOCI files
stash@{1}: On SDCOSMOS-66846: 03ds: key. end-point
stash@{2}: WIP on SDCOSMOS-68083: 302a7b3 Fix
git stash show -p stash@{0}
git stash apply stash@{0}
```

### Remove not tracked file:

```
git clean -n # dry-run, just show what to remove
git clean -d # recursive, go to the internal directories to check
```
