---
tags: ["#binutils"]
---
# readelf

### \*.so are needed:

```
yauheni@pc:/opt/alert/toolchains/qnx-sdp/7.1.0$ readelf -a ./target/qnx7/aarch64le/usr/lib/libmenu.so | grep NEEDED
0x0000000000000001 (NEEDED)            Shared library: [libc.so.5]
0x0000000000000001 (NEEDED)            Shared library: [libgcc_s.so.1]
```
