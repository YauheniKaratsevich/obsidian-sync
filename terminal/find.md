# find

* Execute multiple commands with \`find\`:

```
find -name "*sh" \( -exec echo {} \; -exec grep -rn "read" {} \; \)
find ~/Downloads/ -type f -name "*.pdf" -exec bash -c 'md5=$(md5sum "{}" | cut -f1 -d" "); echo "##$md5";' \;
```

* Find has `-delete` option for deletion:

```
find . -name mysecret -delete
```
