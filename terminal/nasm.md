# nasm

* assemble (compile) asm-file:

```
yauheni@pc:~/crss/adv-c-prog-asm$ nasm -f elf64 ch1.asm
yauheni@pc:~/crss/adv-c-prog-asm$ ll
total 80
drwxr-xr-x 3 yauheni yauheni  4096 Jun 12 14:46 ./
drwxr-xr-x 3 yauheni yauheni  4096 Jun  9 15:15 ../

-rw-r--r-- 1 yauheni yauheni  969 Jun 12 14:44 ch1.asm
-rw-r--r-- 1 yauheni yauheni  1280 Jun 12 14:46 ch1.o
```

* then link it to the binary:

```
yauheni@pc:~/crss/adv-c-prog-asm$ ld ch1.o -o hello
yauheni@pc:~/crss/adv-c-prog-asm$ ./hello
What is your age? yauheni@pc:~/crss/adv-c-prog-asm$
```
