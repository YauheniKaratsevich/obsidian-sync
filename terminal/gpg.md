# gpg

Links:

* [Yubikey + GPG – быстрый старт](https://prudnitskiy.pro/post/2018-08-02-yubikey-gpg/)

* Create keys pair:

```
gpg --full-generate-key
# follow the instructions
```

* Check keys:

```
gpg -k # public (pub)
gpg -K # private (sec)
```

* Export keys (-a means ascii):

```
yauheni@yauheni-pc:~$ gpg --export -a ACBF0CE6AECCCB8E839D762E32E3D5AFAD7DDC72 > ~/Downloads/tmp/pub.key
yauheni@yauheni-pc:~$ gpg --export-secret-keys -a ACBF0CE6AECCCB8E839D762E32E3D5AFAD7DDC72 > ~/Downloads/tmp/private.key
```
