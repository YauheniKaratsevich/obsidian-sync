# jq

### Print all urls of apms:

```
jq -Mr '.apms[].url' $(MANIFEST_FILE)
```

### Select item with .name is bsp:

```
jq -Mr '.apms[] | select(.name=="bsp")' apm-manifest.json
```

### Find the object which has element with value:

```
yauheni@pc:~/work/manufactory-programming-image$ jq -Mr '.apms[] | select(.name=="bsp")' apm-manifest.json
{
  "url": "https://alertinnovation.jfrog.io/artifactory/embedded-snapshots/qnx-bsp-710/bsp_rootfs/alphabot-ifs-3.1.5-97-aarch64.apm",
  "name": "bsp"
}

yauheni@pc:~/work/manufactory-programming-image$ jq -Mr '.apms[] | select(.name=="bsp").url' apm-manifest.json
https://alertinnovation.jfrog.io/artifactory/embedded-snapshots/qnx-bsp-710/bsp_rootfs/alphabot-ifs-3.1.5-97-aarch64.apm
```
