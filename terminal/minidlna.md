# minidlna

### How to run:

```
systemctl start minidlna
```

### How to run, status, stop:

```
service minidlna [start, restart, stop, status, stop]
```

### How to disable (auto start disabling):

```
systemctl disable minidlna.service
```

### Check dlna connection:

```
netstat -tanu | grep 8200
```

### Links:

* [Медиа сервер MiniDLNA на Ubuntu 18](https://internet-lab.ru/minidlna_ubuntu18)
