# xclip

How to copy file content to the [clipboard](https://askubuntu.com/questions/210413/what-is-the-command-line-equivalent-of-copying-a-file-to-clipboard):
```
cat ./myfile.txt|xclip -i -selection clipboard
```
