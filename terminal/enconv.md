# enconv

* encode russian text from Windows encoding (CP1251) to Linux (UTF-8)

```
enconv -x UTF-8 -L ru vv.srt
```
