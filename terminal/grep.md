# grep

* How to exclude dirs and files from the searching:

```
grep -rn TaskCreate --exclude-dir={./lib,./app/microchip} --exclude=tags
```
