# Useful

Copy line by number :         **:14y**     \- copy 14th line
Delete line by number:        **:14d**     - delete 14th line
Delete between (inside):     **:di{**      - delete inside {
Delete between + brackets:     **:da{**      - delete inside {
Delete all before char:        **dtX**      - does not include X
Delete all after cursor:        **{Shift}D**
Comment a group:             **Ctrl+v, up/down, type "//",esc**
Uncomment a group:         **Ctrl+v, up/down,press "x",esc**

|     |     |
| --- | --- |
| **d^** | Удалить все символы от текущей позиции до начала строки |
| **d$** | Удалить все символы от текущей позиции до конца строки |
| **d/word** | Удалить всё от текущей позиции до слова "word" |
| **dfx** | Удалить всё от текущей позиции до символа "x" |

|     |     |
| --- | --- |
| **dtx** | Удалить всё от текущей позиции до символа "x" без "х" |

**I**        добавление в начало строки
**A**       добавление в конец строки

включить отображение скрытых символов  :set list (скрыть  :set nolist)
Скопировать в строку поиска:

1. Шаг  "Ayaw  скопировать в регистр А
2. / ^R A вставить из регистра А  

**qaq** - "clean" register 'a' (put an empty string to register 'a')

\== - Format

* } — команда перемещения, перебрасывает курсор в конец абзаца;
	
* ( — команда перемещения, перебрасывает курсор в начало предыдущего предложения.
	

Страница вниц {Ctrl}+F
n - повторить предыдущий поиск

Поиск строки в опередённых файлах:
find ../ -type f -name \*.bb -exec grep -i -H "do\_image" {} \\; 

:%s/\\<идиот\\>/менеджер/gc     g - глобально, с - с подтверждением
Paste to VIM command line:
1\. yaw - yank all word
2\. : - to go to vim command
3\. Crtl-r - to use registers
4\. Shift - "  - to use default register
