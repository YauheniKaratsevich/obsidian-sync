# VIM

binutils (gcc, gdb, ldd, gprof, as, strings, size, ...)

ctags -R .

vim -t tag

:ts 

Ctrl + \]

Ctrl + O

TagList plugin

:x

:set nu

:set nonu

:set nu!

:w - save

:wq - save and quit

:x - save and quit

dd

u

yy

Shift + V - select

w

:set list

:set nolist

Shift + K

:vs
:split

h

j

k

l

Ctrl +W + hjkl - navigate

:%s/one/two/g

:set hls

:set nohls

:set hls!

Shift + G - to the bottom of the file

gg - to the top

Ctrl + E - scroll

Ctrl + Y - scroll

Shift + # - highlight and navigate

gd - goto defintion

Shift + % - goto matching brace

vimtutor

<https://www.youtube.com/watch?v=Ag1AKIl_2GM&vl=en>
