# Patterns

# In C

* <https://francoischalifour.com/eceman/>
* <https://github.com/adamtornhill/PatternsInC>
* <https://www.adamtornhill.com/Patterns%2520in%2520C%25201.pdf&ved=2ahUKEwiqqLjc78z8AhWGp4sKHUcIAfUQFnoECA4QAQ&usg=AOvVaw2Yz_Odh_L6YOKUE1BwiRxu>

_Patterns in C: Patterns, Idioms and Design Principles_

_<https://github.com/ksvbka/design_pattern_for_embedded_system>_

reusable firmware development
