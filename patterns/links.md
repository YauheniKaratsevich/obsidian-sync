# links

# Useful links:

* [QMonkey/OOC-Design-Pattern](https://github.com/QMonkey/OOC-Design-Pattern)
* [Patterns and practice f](https://youtu.be/YAZatHxwYSk)
* [Object Oriented C Programming Style](https://docs.strongswan.org/docs/5.9/devs/objectOrientedC.html)
* [or embedded TDD in C and C++](https://youtu.be/YAZatHxwYSk)
* [Opaque pointers и инкапсуляция в си.](https://learnc.info/c/opaque_pointers.html)
* <https://eecs280staff.github.io/notes/07_ADTs_in_C.html>
	

Videos:

1. Martin K. Schröder (<https://swedishembedded.com/tag/training/>):
	* [Embedded C Programming Design Patterns Course: Object Pattern](https://youtu.be/24SfZZnwPBs)
2. Bruce Douglass
	* [Design Patterns for Embedded Systems in C (video)](https://youtu.be/S0ODfxXe2UU)

* https://youtu.be/4ryh1bxMn3M
