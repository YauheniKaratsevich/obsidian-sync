# Storage-class specifiers (классы памяти)

Specify _storage duration_ and _linkage_ of objects and functions:

* `**auto**` - automatic duration and no linkage
* `**register**` - automatic duration and no linkage; address of this variable cannot be taken
* `**static**` - static duration and internal linkage (unless at block scope)
* `**extern**` - static duration and external linkage (unless already declared internal)
* `**_Thread_local**` - thread storage duration (since C11, removed in C23)

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Класс памяти | Время жизни | Область видимости | Тип связывания | Точка определения |
| Автоматический | Автоматическое | Блок | Отсутстует | В пределах блока, опционально auto |
| Регистровый | Автоматическое | Блок | Отсутстует | В пределах блока, register |
| Статический без связывания | Статическое | Блок | Отсутстует | В пределах блока, static |
| Статический с внешним связыванием | Статическое | Файл | Внешнее | Вне функций |
| Статический с внутренним связыванием | Статическое | Файл | Внутреннее | Вне функций, static |

Links:

* <https://en.cppreference.com/w/c/language/storage_duration>
* <https://habr.com/ru/companies/jugru/articles/506104/>
