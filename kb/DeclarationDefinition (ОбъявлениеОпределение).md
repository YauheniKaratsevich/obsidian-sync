# Declaration/Definition (Объявление/Определение)

**Объявление (declaration)** только сообщает компилятору, что переменная с таким именем ==где-то существует== (или функция с такой сигнатурой), память не выделяется.
**Определение (definition)** ведет к ==выделению памяти==.

```
extern int x;            // Declaration
int y;                  // Definition
extern int add(int, int);// Declaration
int sub(int, int);      // Declaration
int cnt(int a) {        // Definition and implementation
    return a + 1;
}
```

```
// Function declaration
int sqr(int x);

// Function definition
int sqr(int x) { return x * x; }

// Variable declarations
extern int n;
struct A { static int n; };

// Variable definitions
int n;
int A::n;
```
