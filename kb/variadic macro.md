# variadic macro

Link: <https://gcc.gnu.org/onlinedocs/cpp/Variadic-Macros.html>

```
#define nbiotPRINTF(fmt, ...) configPRINTF(("\033[31m>>Client %d<<\033[0m"fmt,(int) xNbiotMQTTHandle, ##__VA_ARGS__ ))
```
