# extern "C" {..}

Сообщить линкеру при сборке С++ кода, что функции и переменные надо линковать **==в Си стиле==**, а именно, не производить ==name mangling.==
```
// Declare printf with C linkage.
extern "C" int printf(const char *fmt, ...);

//  Cause everything in the specified
//  header files to have C linkage.
extern "C" {
    // add your #include statements here
    #include <stdio.h>
}
```
