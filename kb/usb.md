# usb

### **User Mode Driver:**

	[The USB Filesystem (usbfs)](https://www.kernel.org/doc/html/v4.10/driver-api/usb.html#the-usb-filesystem-usbfs)
	
* [USB Device Filesystem](http://www.linux-usb.org/USB-guide/x173.html)
	[libusb](https://github.com/libusb/libusb#libusb)
	
* [essential linux device drivers](http://www.embeddedlinux.org.cn/essentiallinuxdevicedrivers/)

### **Protocol:**

* [Популярно о USB](http://www.gaw.ru/html.cgi/txt/interface/usb/instr/index.htm)
* [USB made simple](https://www.usbmadesimple.co.uk/)
