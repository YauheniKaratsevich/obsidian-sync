# profiling

### 

* [gprof](https://ftp.gnu.org/old-gnu/Manuals/gprof-2.9.1/html_mono/gprof.html)

```
1. Compile with `-pg` option:
# gcc -Wall -std=c99 -pg test_gprof.c -o test_gprof
# ls
...
gmon.out
# gprof test_gprof gmon.out > profile-data.txt
```

* [perf](https://perf.wiki.kernel.org/index.php/Main_Page)

```
Installation: # apt-get install linux-tools-common
1. # perf record ./a.out
2. # perf report ./a.out
```
