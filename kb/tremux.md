# tremux

## How to connect:

* Run `termux` on the phone
* Run `sshd` in the `termux`
* Connect to the phone by following:

```
yauheni@pc:~$ ssh u0_a183@192.168.68.115 -p8022
```
