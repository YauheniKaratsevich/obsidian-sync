# How to mount image with several partitions (примонтировать образ с несколькими партициями)

1. Check the image for partitions:

```
yauheni@yauheni-pc:/$ fdisk -l from-sd-card.img
Disk from-sd-card.img: 14.9 GiB, 15931015168 bytes, 31115264 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x1eb9e02c

Device            Boot Start      End  Sectors  Size Id Type
from-sd-card.img1      8192    93802    85611 41.8M  c W95 FAT32 (LBA)
from-sd-card.img2      98304 31116287 31017984 14.8G 83 Linux
```

2. Calculate the start address of partitions:

```
For from-sd-card.img1: 512 * 8192 = 4194304
For from-sd-card.img2: 512 * 98304 = 50331648
```

3. Mount partitions:

```
yauheni@yauheni-pc:/$ sudo mkdir /mnt/usb1
yauheni@yauheni-pc:/$ sudo mount -o loop,offset=4194304 from-sd-card.img /mnt/usb1
yauheni@yauheni-pc:/$ sudo mkdir /mnt/usb2
yauheni@yauheni-pc:/$ sudo mount -o loop,offset=50331648 from-sd-card.img /mnt/usb2
```
