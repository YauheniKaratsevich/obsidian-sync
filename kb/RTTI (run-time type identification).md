# RTTI (run-time type identification)

RTTI позволяет определить тип объекта во время исполнения программы.
Используется функция `typeid`

![[./_resources/RTTI_(run-time_type_identification).resources/image.png]]
